# SPDX-License-Identifier: MIT

activate :livereload

set :css_dir, 'stylesheets'
set :js_dir, 'javascripts'
set :images_dir, 'images'

configure :build do
  activate :minify_css
  activate :minify_javascript
  activate :minify_html
  activate :gzip
end

activate :directory_indexes

require 'builder'

Time.zone = "America/Los_Angeles"

set :markdown_engine, :redcarpet
set :markdown, :fenced_code_blocks => true, :smartypants => true

activate :blog do |blog|
  blog.summary_separator = /READMORE/
end
activate :syntax

page "/sitemap.xml", :layout => false
page "/feed.xml", :layout => false
