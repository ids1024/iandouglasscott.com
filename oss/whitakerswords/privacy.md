---
title:  Whitaker's Words for Android
# Copyright Ian Douglas Scott, all rights reserved
---

# Privacy Policy

[ Whitaker's Words for Android](https://play.google.com/store/apps/details?id=com.ids1024.whitakerswords) collects no data and does not communicate with any remote servers.
