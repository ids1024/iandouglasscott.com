---
SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: Wikicurses
summary: Wikicurses is a command line Wikipedia/Mediawiki client with a curses interface.
language: Python
# repository:
repository-name: Github
repository-url: https://github.com/ids1024/wikicurses
# download:
download-name: PyPI
download-url: https://pypi.python.org/pypi/Wikicurses
---

Wikicurses is a command line Wikipedia/Mediawiki client with a curses interface.
