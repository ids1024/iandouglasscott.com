---
SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: Isbfc
description: Isbfc is an optimizing compiler for the trivial Brainfuck programming language, targeting the x86_64 architecture. Like Brainfuck, it is of no practical value, but writing a well optimized compiler for it is nevertheless an entertaining challenge.
language: Rust
# repository:
repository-name: Github
repository-url: https://github.com/ids1024/isbfc/
---

Isbfc is an optimizing compiler for the trivial Brainfuck programming language, targeting the x86_64 architecture. Like Brainfuck, it is of no practical value, but writing a well optimized compiler for it is nevertheless an entertaining challenge.

