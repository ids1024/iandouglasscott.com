---
SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: Whitaker's Words
summary: A port of Whitaker's Words to Android.
language: Java
repository:
# repository:
repository-name: Github
repository-url: https://github.com/ids1024/whitakers-words-android
# download:
download-name: Google Play
download-url: https://play.google.com/store/apps/details?id=com.ids1024.whitakerswords
---
A port of Whitaker's Words to Android.

Whitaker's Words is a very useful morphological analysis tool and dictionary for Latin written by William Whitaker. This is a port of Words to the Android platform, wrapping the console interface with a touch-friendly GUI. More information about Words in general can be found at [Whitaker's old website](http://archives.nd.edu/whitaker/words.htm). In addition to the original console interface and my Android port, there is also [an online version](http://www.archives.nd.edu/words.html).

