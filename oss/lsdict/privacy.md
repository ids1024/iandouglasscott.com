---
title: Lewis and Short Latin Dictionary Privacy Policy
# Copyright Ian Douglas Scott, all rights reserved
---

# Privacy Policy

[Lewis and Short Latin Dictionary](https://play.google.com/store/apps/details?id=com.ids1024.lsdict) collects no data and does not communicate with any remote servers.
