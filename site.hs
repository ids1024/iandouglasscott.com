{-# LANGUAGE OverloadedStrings, BlockArguments #-}

-- SPDX-License-Identifier: MIT

-- TODO: Feed assumes dates are UTC?
-- Indicate license on page?
-- Link to feed?

import Control.Monad.Error.Class
import qualified Data.ByteString.Lazy as LBS
import Data.List
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Yaml
import qualified Data.HashMap.Strict as HashMap
import Hakyll
import Hakyll.Web.Sass
import System.FilePath
import Data.Time.Format
import Data.Time.Clock
import Data.Time.LocalTime
import Text.Pandoc.Options
import Skylighting.Loader (loadSyntaxesFromDir)
import Skylighting.Types (SyntaxMap)

main :: IO ()
main = do
    writerOptions <- writerOptions
    hakyllWith config do
        match "images/**" do
            route idRoute
            compile copyFileCompiler

        gzipMatch "css/*.sass"
           (setExtension "css")
           (fmap compressCss <$> sassCompiler)

        gzipMatch "css/*.css"
           idRoute
           (fmap compressCss <$> getResourceBody)

        gzipMatch "oss/**"
            cleanRoute
            (pandocCompilerWith defaultHakyllReaderOptions writerOptions
                >>= loadAndApplyTemplate "templates/oss.html"    postCtx
                >>= loadAndApplyTemplate "templates/default.html" postCtx)

        gzipMatchMetadata "posts/**" notDraft
            postRoute
            (pandocCompilerWith defaultHakyllReaderOptions writerOptions
                >>= saveSnapshot "content"
                >>= loadAndApplyTemplate "templates/post.html"    postCtx
                >>= loadAndApplyTemplate "templates/default.html" postCtx)

        gzipMatch "index.html"
            idRoute
            do
                posts <- recentFirst =<< loadAll ("posts/**" .&&. hasNoVersion)
                let indexCtx =
                        listField "posts" postCtx (return posts) <> siteCtx
                getResourceBody
                    >>= applyAsTemplate indexCtx
                    >>= loadAndApplyTemplate "templates/default.html" indexCtx

        gzipMatch "oss.html"
            cleanRoute
            do
                projects <- loadAll ("oss/*" .&&. hasNoVersion)
                let indexCtx =
                        listField "projects" postCtx (return projects) <>
                        siteCtx
                getResourceBody
                    >>= applyAsTemplate indexCtx
                    >>= loadAndApplyTemplate "templates/default.html" indexCtx

        match "templates/*" $ compile templateBodyCompiler

        gzipCreate ["feed.xml"]
            idRoute
            do
                posts <- recentFirst =<< loadAllSnapshots ("posts/**" .&&. hasNoVersion) "content"
                renderAtom feedConfiguration feedCtx posts

        gzipCreate ["sitemap.xml"]
            idRoute
            do
                posts <- recentFirst =<< loadAll ("posts/**" .&&. hasNoVersion)
                projects <- loadAll ("oss/*" .&&. hasNoVersion)
                let pages = posts <> projects
                    sitemapCtx =
                        constField "root" root <> 
                        listField "pages" postCtx (return pages)
                makeItem "" >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx

config :: Configuration
config = defaultConfiguration
    { deployCommand = "rsync -rv --delete _site/ rh1.ids1024.com:/srv/http/iandouglasscott"
    }

notDraft :: Metadata -> Bool
notDraft = isNothing . lookupString "is-draft"

navItems :: [(String, String)]
navItems = [
    ("Blog", "/"),
    -- TODO: New photos and poetry sections, other creativity
    -- ("Open Source Software", "/oss"),
    ("Github", "https://github.com/ids1024"),
    ("Mastodon", "https://fosstodon.org/@ids1024")
    -- atom feed?
    -- about page?
    ]

navItems2 :: Item String -> Compiler [Item (String, String)]
navItems2 i = do
    url <- getMetadataField (itemIdentifier i) "url"
    mapM makeItem navItems

stripDotSlash url = maybe url ("/"++) $ stripPrefix "/./" url

navCtx :: Context (String, String)
navCtx = field "navName" (return . fst . itemBody) <>
         field "navUrl" (return . snd . itemBody) <>
         field "navCurrent" (\_ -> noResult "") <>
         field "navRel" (\i -> do
          if fst (itemBody i) == "Mastodon"
             then return ""
             else noResult "")
         {-
         field "navCurrent" (\i -> do
             let navUrl = snd (itemBody i)
             route <- getRoute (itemIdentifier i)
             let url = stripDotSlash $ dropIndexHtmlStr $ maybe (fail "") toUrl route
             if (url == navUrl) || (navUrl /= "/" && isPrefixOf navUrl url)
             then return ""
             else noResult "")
         -}

postRoute :: Routes
postRoute = metadataRoute (customRoute . createPostRoute)

createPostRoute :: Metadata -> Identifier -> FilePath
createPostRoute metadata ident = date </> basename </> "index.html"
    where
        date = formatTime defaultTimeLocale "%Y/%m/%d" time
        time = fromJust $ lookupPublished metadata
        basename = takeBaseName $ toFilePath ident

-- Can't call getItemUTC without Monad; use one of the formats it also supports
-- And parse as local time.
parseDateField :: String -> LocalTime
parseDateField = parseTimeOrError False defaultTimeLocale "%Y-%m-%d %H:%M:%S%Z"

lookupPublished :: Metadata -> Maybe LocalTime
lookupPublished = fmap parseDateField . lookupString "published"

myDateField :: String -> String -> Context a
myDateField name fmt = field name (\i -> do
        metadata <- getMetadata (itemIdentifier i)
        maybe
             (fail $ "no 'published' field in " ++ toFilePath (itemIdentifier i))
             (return . formatTime defaultTimeLocale fmt)
             (lookupPublished metadata)
    )

postCtx :: Context String
postCtx =
    constField "root" root <> 
    myDateField "date" "%B %e, %Y" <>
    myDateField "datetime" "%Y-%m-%d" <>
    teaserField "teaser" "content" <>
    dropIndexHtml "url" <>
    listFieldWith "discussion" discussionCtx (\i -> do
            value <- getMetadataField (itemIdentifier i) "discussion"
            maybe
                (noResult "")
                (\value -> case Data.Yaml.decodeEither' (TE.encodeUtf8 $ T.pack value) of
                    Left err -> error (Data.Yaml.prettyPrintParseException err)
                    Right values -> mapM makeItem $ HashMap.toList values
                )
                value
        ) <>
    siteCtx

siteCtx :: Context String
siteCtx =
    listFieldWith "navItems" navCtx navItems2 <>
    defaultContext

discussionCtx :: Context (String, String)
discussionCtx =
    bodyMapField "name" fst <> bodyMapField "link" snd
    where
        bodyMapField name f = field name (return . f . itemBody)

feedCtx :: Context String
feedCtx = postCtx <> bodyField "description"

feedConfiguration :: FeedConfiguration
feedConfiguration =
    FeedConfiguration
        { feedTitle       = "Ian Douglas Scott's Blog"
        , feedDescription = ""
        , feedAuthorName  = "Ian Douglas Scott"
        , feedAuthorEmail = ""
        , feedRoot        = root
        }

root :: String
root = "https://iandouglasscott.com"

-- https://www.rohanjain.in/hakyll-clean-urls
cleanRoute :: Routes
cleanRoute = customRoute $ (</> "index.html") . dropExtension . toFilePath

-- https://aherrmann.github.io/programming/2016/01/31/jekyll-style-urls-with-hakyll
dropIndexHtml :: String -> Context a
dropIndexHtml key = mapContext dropIndexHtmlStr (urlField key) where

dropIndexHtmlStr :: String -> String
dropIndexHtmlStr url = case splitFileName url of
    (p, "index.html") -> takeDirectory p
    _                 -> url

gzipMatch :: Pattern -> Routes -> Compiler (Item String) -> Rules ()
gzipMatch = gzipRules . match

gzipMatchMetadata :: Pattern -> (Metadata -> Bool) -> Routes -> Compiler (Item String) -> Rules ()
gzipMatchMetadata = curry $ gzipRules . (uncurry matchMetadata)

gzipCreate :: [Identifier] -> Routes -> Compiler (Item String) -> Rules ()
gzipCreate = gzipRules . create

-- Based on
-- https://blog.debiania.in.ua/posts/2016-08-05-pre-compressing-files-with-hakyll.html
gzipRules :: (Rules () -> Rules ()) -> Routes -> Compiler (Item String) -> Rules ()
gzipRules rules routes compiler = do
    rules do
        route routes
        compile compiler

    rules $ version "gzipped" do
        route $ composeRoutes routes (customRoute ((<> ".gz") . toFilePath))
        compile do
            id <- getUnderlying
            body <- loadBody (setVersion Nothing id)
            makeItem body >>= gzip

    where
        gzip :: Item String -> Compiler (Item LBS.ByteString)
        gzip = withItemBody (unixFilterLBS "gzip" ["--best"] . stringToLBS)

stringToLBS :: String -> LBS.ByteString
stringToLBS = LBS.fromStrict . TE.encodeUtf8 . T.pack

additionalSyntaxMap :: IO SyntaxMap
additionalSyntaxMap = do
    syntax <- loadSyntaxesFromDir "syntax"
    case syntax of
        Right value -> return value
        Left err -> error err

syntaxMap :: IO SyntaxMap
syntaxMap = do
    syntax <- additionalSyntaxMap
    return $ writerSyntaxMap defaultHakyllWriterOptions `mappend` syntax

writerOptions :: IO WriterOptions
writerOptions = do
    syntaxMap <- syntaxMap
    return defaultHakyllWriterOptions {
        writerSyntaxMap = syntaxMap
    }
