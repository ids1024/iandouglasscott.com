#!/usr/bin/env python3

# SPDX-License-Identifier: MIT

import os
import re
import sys
import fnmatch 

if '--full' in sys.argv:
    from spdx.utils import LicenseListParser
    license_list_parser = LicenseListParser()
    license_list_parser.build()
else:
    license_list_parser = None

SPDX_REGEX = r".*SPDX-License-Identifier:\s*(.*)$"
ALL_RIGHTS_RESERVED = "Copyright Ian Douglas Scott, all rights reserved"
SKIP_PATHS = ("./.*",
              "./README.md",
              "./build",
              "./_cache",
              "./_site",
              "./dist-newstyle",
              "./dist",
              "./cabal.project.freeze",
              )

exitcode = 0

def checkdir(directory):
    global exitcode

    for f in os.listdir(directory):
        path = f"{directory}/{f}"

        if any(fnmatch.fnmatch(path, i) for i in SKIP_PATHS):
            print(f"Skipping '{path}'")
            continue
        
        license = None

        if os.path.isdir(path):
            if os.path.exists(f'{path}/LICENSE'):
                path_type = "directory"
                license_path = f'{path}/LICENSE'
            else:
                checkdir(path)
                continue
        else:
            path_type = "file"
            license_path = path

        try:
            with open(license_path, encoding="utf8") as src_file:
                for l in src_file:
                    m = re.match(SPDX_REGEX, l)
                    if m is not None:
                        license = m.group(1)
                        if license_list_parser is not None:
                            license = license_list_parser.parse(license).full_name
                        break
                    if ALL_RIGHTS_RESERVED in l:
                        license = "All rights reserved"
        except UnicodeDecodeError:
            sys.stderr.write(f"{license_path} is not UTF-8\n")
            exitcode = 1

        if license is None:
            sys.stderr.write(f"No license for {path_type} '{path}'\n")
            exitcode = 1
        else:
            print(f"License for {path_type} '{path}' is '{license}'")

checkdir('.')

sys.exit(exitcode)
