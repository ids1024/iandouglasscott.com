#!/bin/bash

# SPDX-License-Identifier: MIT

size=300
#newname="${1%.*}-thumb.${1##*.}"
newname="${1%.*}-thumb.jpg"
convert "$1" -thumbnail $sizex$size^ -gravity center -extent $sizex$size "$newname"
