iandouglasscott.com
===================

```
bundle install
bundle exec middleman
```

Licensing
---------
This repository isn't under a single license. This isn't particularly convenient, but it seems like a good compromise over just keeping the code private. In general, the code is MIT licenced, the blog posts CC-BY-NC-ND-4.0, and some content (like photography) is "all rights reserved".

As far as I'm aware, "all rights reserved" doesn't really mean anything under modern copyright law (since copyright exists implicitly without any declaration), but it seems to be a popular way to refer to the legal "default" licensing terms.

SPDX identifiers are included in files to identify the license. Check those or run `./scripts/license-test.py` for more information on licensing.

My particular choice of Creative Commons license (CC-BY-NC-ND-4.0) is pretty restrictive (which seems reasonable for blog posts), but I may be willing to allow less restrictive terms if someone shows interest for some reason.
