#!/bin/sh

# SPDX-License-Identifier: MIT

date '+%Y-%m-%d %H:%M:00%Z'
