---
SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: Google Summer of Code
published: 2017-09-04 16:45:00PDT
---

Perhaps I *could* write a blog here...

But as a start, my post [GSoC Project: Making Redox Self-hosting, Final Summary](https://redox-os.org/news/gsoc-self-hosting-final/) on the Redox OS blog descibes the Google Summer of Code project I did this summer, and links to my eariler posts there about it.

<!--more-->
