---

SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: Playing MIDI Files with Gleam and Alsa Sequencer
published: 2100-01-01 00:00:00PDT
is-draft: true

---
<!--
aplaymidi --list
gleam run 128:0 test.mid

add rust code?
cargo build --release
-->

<!-- talk about issues I ran into? -->

I'm generally a fan of the Rust programming language. One thing I like about Rust is that it's a language that can be used for everything. Programming microcontrollers? Sure! Writing an OS kernel? Definitely. Desktop application? Could be a good choice. Front-end web scripting? It's one of the best languages for targeting WebAssembly.

Though when I find myself writing `Fn` generics with [higher-ranked trait bounds](https://doc.rust-lang.org/nomicon/hrtb.html) I do sometimes wonder if for some of these uses it could be useful to have a "higher-level" language that is garbage collected but offers the other features I like from Rust (structural matching, static types, etc). And maybe is a bit more functional. OCaml seems like the main option for an "impure statically typed programming language", though it definitely has some of its own quirks.

I've also been interesting in Erlang (and Elixir), but the choice of the language to use dynamic typing seems a bit odd given it's other priorities. (Perhaps that reflects the industry trend at the time it was written, while statically typed "higher-level" languages are more in the vogue now?).

But I've heard of this newer language called Gleam, which is implemented in Rust for the Erlang BEAM VM. And looks a bit like Rust syntactically, but is garbage collected and more of a functional programming language. Given this is exactly something I've been asking for, I have to at least give it a try. And since [Gleam 1.0 was released](https://gleam.run/news/gleam-version-1) earlier this year, it seems like a good time to take a look.

<!--more-->

Gleam seems fairly approachable looking at the [Gleam tour](https://tour.gleam.run). At least with background in Rust and to a lesser extent more functional programming languages.

**NOTE**: This is not a tutorial on either MIDI or Gleam, and should not be construed as the correct way of doing anything.

### Standard MIDI Files

[Standard MIDI files](https://midi.org/standard-midi-files) are defined as a sequence of "chunks", consisting of a 4 character type, 32-bit big endian length, and the data contained in the chunk. The first chunk of the file is a `"MThd"` chunk containing the MIDI header.

```Gleam
type Chunk {
  Chunk(chunk_type: String, body: BitArray)
}

fn parse_chunk(bits: BitArray) -> #(Chunk, BitArray) {
  let assert <<chunk_type:bytes-4, len:32, rest:bytes>> = bits
  let assert <<body:bytes-size(len), rest:bytes>> = rest
  let assert Ok(chunk_type) = bit_array.to_string(chunk_type)
  #(Chunk(chunk_type, body), rest)
}
```

[Gleam's bit syntax](https://tour.gleam.run/data-types/bit-arrays) is based on [Erlang bit syntax]https://www.erlang.org/doc/system/bit_syntax.html), but seems to be a bit different. It can be used to generate bit arrays, or to match them, as we do here. Integers default to big endian, which matches what MIDI uses.

The Erlang documentation mentions that you can parse a size, then use that later in the bit expression:

<!-- 
{ok, Data} = file:read_file("test.mid").
-->
```erlang
<<ChunkType:4/bytes, Len:32, Body:Len/bytes, Rest/bytes>> = Bits.
```

This doesn't seem to work in Gleam currently, so we have to match more than once.

We can then use this to parse the header. The header chunk has a length of 6 bytes, consisiting of 3 integer files. We can define a type for the header, and also use bit syntax to match of the body of the header chunk.

````Gleam
type Header {
  Header(format: Int, ntrks: Int, division: Int)
}

fn parse_header(bits: BitArray) -> #(Header, BitArray) {
  let assert #(Chunk("MThd", <<format:16, ntrks:16, divison:16>>), rest) =
    parse_chunk(bits)
  #(Header(format, ntrks, divison), rest)
}
````

### Error Handling

Gleam, unlike Erlang, doesn't use exceptions. So we shouldn't be using `let assert` if we want proper error handling. So like in Rust, the correct thing to do would be to return a `Result`.

```Gleam
type MidiError {
  InvalidChunk
  HeaderError
}

fn parse_chunk(bits: BitArray) -> Result(#(Chunk, BitArray), MidiError) {
  case bits {
    <<chunk_type:bytes-4, len:32, rest:bytes>> -> {
      case rest {
        <<body:bytes-size(len), rest:bytes>> -> {
          case bit_array.to_string(chunk_type) {
            Ok(chunk_type) -> Ok(#(Chunk(chunk_type, body), rest))
            Error(err) -> Error(InvalidChunk)
          }
        }
        _ -> Error(InvalidChunk)
      }
    }
    _ -> Error(InvalidChunk)
  }
}

fn parse_header(bits: BitArray) -> Result(#(Header, BitArray), MidiError) {
  use #(chunk, rest) <- result.try(parse_chunk(bits))
  case chunk {
    Chunk("MThd", <<format:16, ntrks:16, divison:16>>) -> {
      Ok(#(Header(format, ntrks, divison), rest))
    }
    _ -> Error(HeaderError)
  }
}
```

At least in `parse_chunk`, this feels a bit verbose. It's complicated a bit by the fact Gleam doesn't have early returns, so we need a `case` and indentation to handle a fallible match. In `parse_header` we can make use of Gleam's interesting and unusual [use syntax](https://tour.gleam.run/advanced-features/use), to avoid this, but as far as I can tell there's no obvious way to simplify this. In Rust I'd use the `let else` syntax and an early return, which isn't especially elegant but is often handy.

I'll skip proper error handling for the rest of this, but this is what one *should* do.

## Midi Tracks
After the header, a MIDI file contains a sequence of track chunks, tagged with `MTrk`. These consist of a sequence events, which each have a "delta time".

We can just use `parse_chunk` again:

```Gleam
type Track {
  Track(events: List(TrackEvent))
}

fn parse_track(bits: BitArray) -> #(Track, BitArray) {
  let assert #(Chunk("MTrk", body), rest) = parse_chunk(bits)
  let events = parse_events(body, None)
  #(Track(events), rest)
}
```

But `parse_events` is where things get complicated.

First, each MIDI event has a "delta time". But it's encoded in a variable-length format. It uses the first bit of each byte to encode if we should continue parsing the integer:

```Gleam
fn parse_varlength_int_inner(bits: BitArray) -> #(List(Int), BitArray) {
  let assert <<continue:1, byte:7, rest:bytes>> = bits
  case continue {
    0 -> #([byte], rest)
    1 -> {
      let #(bytes, rest) = parse_varlength_int_inner(rest)
      #([byte, ..bytes], rest)
    }
    _ -> panic
  }
}

fn parse_varlength_int(bits: BitArray) -> #(Int, BitArray) {
  let #(bytes, rest) = parse_varlength_int_inner(bits)
  #(
    list.fold(bytes, 0, fn(value, byte) {
      int.bitwise_or(int.bitwise_shift_left(value, 7), byte)
    }),
    rest,
  )
}
```

Not sure if there's a simpler way to write this, but whatever.

Next a midi message contains a "status byte" indicating what type of event it is. But MIDI (either files, or the wire format) also supports a "running status", where the status byte can be omitted if its the same as the previous status byte, to save space. So first we need to recognize if the first bit after the delta time is `1`, indicating this is a status byte.

Table 1 in [the MIDI 1.0 specification](https://midi.org/midi-1-0-core-specifications) then tells us how many data bytes go with each status byte.

```Gleam
type Event {
  Event(status: Int, data: BitArray)
}

fn data_bytes_for_status_byte(status: Int) -> Int {
  case int.bitwise_shift_right(status, 4) {
    0x8 -> 2
    0x9 -> 2
    0xA -> 2
    0xB -> 2
    0xC -> 1
    0xD -> 1
    0xE -> 2
    _ -> panic
  }
}

fn parse_midi_msg(
  bits: BitArray,
  running_status: Option(Int),
) -> #(Event, BitArray) {
  let #(status, rest) = case bits {
    <<1:1, status:7, rest:bytes>> -> {
      #(int.bitwise_or(0x80, status), rest)
    }
    _ -> {
      let assert Some(status) = running_status
      #(status, bits)
    }
  }
  let n_data_bytes = data_bytes_for_status_byte(status)
  let assert <<data:bytes-size(n_data_bytes), rest:bits>> = rest
  #(Event(status, data), rest)
}
```

### ALSA MIDI

<!-- TODO -->

Actually if we connected a MIDI Jack (with an optoisolator) to a serial port (or more likely USB-serial adapter), we'd need less code. Though it would still need to call into native code to call `tccgetattr`/`tcsetattr` to set the baud rate.

### Complete Code
<!-- TODO -->

### Conclussion

I'd definitely say Gleam is a less "general-purpose" langauge than Rust. It's probably not a good choice of language if your code consists mostly of `ioctl` calls. It definitely can work if your program needs to do but mostly does other things.

Gleam has chosen to make the language "simple" in various ways, while offering some more unusual features like the `use` syntax.

The language and especially library ecosystem are obviously less mature than older languages, but the documentation and tooling are surprisingly good. Straightforward build system, autoformatting, good compiler error messages, and even a language server (though I haven't tried that).

### Future Possibilties

This code could definitely be improve to handle all MIDI features correctly, with error handling. Though doing things "right" is less exciting.

Now that I have some Gleam code involving MIDI, I should try running it on [AtomVM](https://www.atomvm.net), an implementation of the Erlang BEAM for microcontrollers. It should be easy enough to use the [UART module](https://www.atomvm.net/doc/main/apidocs/erlang/eavmlib/uart.html) it includes...

A definining feature of Erlang and the BEAM VM is it's support for concurrency based on message passing between lightweight "processes". In Gleam we can do this with the [gleam-otp](https://hexdocs.pm/gleam_otp/index.html) library. This is probably a big part of why one would use Gleam, and I haven't touched it here. I still need to come up with a good project to make use of that, but the bit syntax matching feature is also a pretty unique feature you don't see in many languages.

<!-- Future potential: AtomVM -->
<!-- Show MIDI module photo -->
