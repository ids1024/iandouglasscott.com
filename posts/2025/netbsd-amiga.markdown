---

SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: Installing NetBSD 10 for Amiga on FS-UAE 
published: 2100-01-01 00:00:00PDT
is-draft: true

---

For whatever reason, I've found myself a bit interested in the m68k, the Comodore Amiga, and running NetBSD on an Amiga. Well, an emulated one anyway.

It took me a bit longer than expected to actually get that working. So I think I'll write up what steps are required before talking about compiling any wild things written in Rust for m68k NetBSD using `rustc_codegen_gcc` (an upcoming blog post... maybe).

**Warning**: This isn't really a good tutorial, if you are interested in trying the NetBSD Amiga port. But this is how I ended up doing things, and may be useful information.

<!--more-->

### Getting NetBSD

[NetBSD/NetBSD-10.0/amiga/INSTALL.html](https://cdn.netbsd.org/pub/NetBSD/NetBSD-10.0/amiga/INSTALL.html) has instructions to install NetBSD on an Amiga. Though some of the instructions aren't very up-to-date. It mentions `installation/floppy`, which it seems was removed in... NetBSD 1.1? Maybe I could submit a patch for the documentation somewhere to just remove that, but since it's been outdated since before I was born, I think it's a holy text at this point.

Anyway, we want [NetBSD/NetBSD-10.0/amiga/installation/miniroot/miniroot.fs.gz](https://cdn.netbsd.org/pub/NetBSD/NetBSD-10.0/amiga/installation/miniroot/miniroot.fs.gz). Which provides an installer that we can write to a hard disk partition. This is the recommended way to install on an Amiga.

### Creating and partitioning hard disk image

<!-- screenshot of partitioning tool in amigaos -->



Using the partitioning tool in Amiga Workbench is a real pain. Partition manager GUIs have actually come a long way since then. And then we need to get the `miniroot.fs` file onto some virtual drive that can be read under Workbench, and use the commandline `xstreamtodev` tool NetBSD also provides...

It turns out `parted` actually can create Amiga partion tables! But it doesn't seem to be able to set the number of "BootBlocks" like the NetBSD install docs say we have to? Oh well, we can work around that in a horrid cursed way.

To write the minirootfs, We might be able to loopback mount the disk image and use `dd` to write to the partition. But it seems Linux distro kernels don't necessarily enable `CONFIG_AMIGA_PARTITION`. We can just `dd` with a seek. This should work on any OS with `parted` and other standard Unix tools.

```bash
set -e

filename=netbsd-10.hdf

truncate -s 1GiB $filename
/usr/sbin/parted --script $filename \
	mklabel amiga \
	mkpart Swap 1MiB 32MiB \
	set 1 boot on \
	mkpart Root 32MiB 1023MiB
dd conv=notrunc if=miniroot.fs of=$filename seek=1MiB

# Set `Bootblocks` for first partition
boot_part_offset=$(grep --only-matching --byte-offset --max-count=1 --text PART $filename | head -n1 | cut -d : -f 1)
printf '\x10' | dd conv=notrunc of=$filename bs=1 seek=$(expr $boot_part_offset + 204 + 3)
# Checksum hack
printf '\x11' | dd conv=notrunc of=$filename bs=1 seek=$(expr $boot_part_offset + 11)
```

This is cursed and fragile, but seems to work as an automated and reproducible way to create an Amiga hard disk image flashed with the miniroot fs. 

### Emulating an Amiga with fs-uae

Using the `fs-uae` for Linux distro archives, I had some trouble with networking. It's actually been a while since I tried that, but the last release of fs-uae does some awkward thing using "qemu-uae" for networking.

[The git repository of `fs-uae`](https://github.com/FrodeSolheim/fs-uae) hasn't been updated lately, but has some nice changes, including a cleaner approach to networking using `libslirp`. Though also the GUI refresh was never finished, so it has some issues. For some reason I had issues with the edges of the screen being cut of, when booting BSD? Maybe it just tries to simulate overscan and NetBSD isn't accounting for that..

Anyway, personally what I've found to work best is the git version of `fs-uae` with this change to prevent anything from being cut off:

```diff
diff --git a/src/od-fs/fsvideo.cpp b/src/od-fs/fsvideo.cpp
index 787ff8a0..271aae92 100644
--- a/src/od-fs/fsvideo.cpp
+++ b/src/od-fs/fsvideo.cpp
@@ -2299,10 +2299,10 @@ bool uae_fsvideo_renderframe(int monid, int mode, bool immediate)
                        frame->limits.w = cw;
                        frame->limits.h = ch;

-                       frame->limits.x = 48;
-                       frame->limits.y = 22;
-                       frame->limits.w = 692;
-                       frame->limits.h = 540;
+                       frame->limits.x = 0;
+                       frame->limits.y = 0;
+                       frame->limits.w = 752;
+                       frame->limits.h = 576;
                }
```

To run an Amiga emulator, you'll need an Amiga ROM. You can purchase [Amiga Forever](https://www.amigaforever.com/) which includes Amiga ROMs, floppy images, and games. Or you can obtain the required file by some other means. Regardless, if you're reading this for some reason but aren't familiar with the Amiga, it's interesting to give Amiga Workbench and other Amiga software and games a try.

Anyway, I'm using `amiga-os-310-a4000.rom`. Targetting the A4000 seems reasonable, since we need a more advanced processor like the 68040 that has an MMU, and the A4000 has the AGA graphics chipset. The most advanced (official) graphics on an Amiga, so it should be useful for testing what graphics we can get in the NetBSD Amiga port.

We can create a `netbsd-10.fs-uae` file:

```ini
[fs-uae]
amiga_model = A4000
chip_memory = 2048
cpu = 68040
fast_memory = 8192
hard_drive_0 = netbsd-10.hdf
hard_drive_0_controller = ide0
kickstart_file = $HOME/Amiga Files/Shared/rom/amiga-os-310-a4000.rom
zorro_iii_memory = 262144
network_card = a2065

```

We can start this with `fs-uae netbsd-10.fs-uae`.

### Running the installer

When prompted by the bootloader, we can load from the miniroot with:
```
root device: wd0d
dump device: none
```
And leave the defaults for "filesystem" and "init path". Then choose a keyboard layout, and select "install".

<!-- Drop to shell, format root partition. -->

We can select `wd0` as the root disk. It doesn't seem to choose the right root partition and swap partition numbers by default, it lets us edit the fstab afterwards.

```
wd0e    /
wd0d    swap
```

Then we need to configure the network. The hostname and "DNS domain name" shouldn't really matter. Then we need to configure interface `le0`. NetBSD should be ablet o use DHCP, but I guess the Amiga version at least uses an installer that doesn't handle that? Whatever. We want:

```
IP address: 10.0.2.15
Netmask: 255.255.255.0
IP address of the default route: 10.0.2.2
IP address of primary name server: 10.0.2.3
```

It fails to format the root partition. It seems `parted` can't set the partition type correctly for Amiga filesystem? Oh well, still easier than trying to partition in Amiga Workbench. The installer drops to a shell.

```
newfs -I /dev/wd0e
```

Then `exit` seems to drop back to the start of the install? Ugh, I still need to find a way to better streamline this process... (I wonder if it would be possible to bypass the whole installer and create a disk image on something other than the emulated environment? Probably not that hard, particularly if running NetBSD.)

Oh well, back through everything. It seems to remember what we said for things like the IP address, but not all settings.

Not we get to the installing part of the installer! It asks us "Install from (f)tp, (t)ape, (C)D-ROM, (N)FS, or local (disk)?".

We could choose connect an emulated CD drive to the system, but FTP works.

```
Server IP: ftp.netbsd.org
Login: anonymous
Password: anonymous
Server directory: 
```

### The result

### Networking and package management

### X11

<!-- more than 1 bit color? -->
