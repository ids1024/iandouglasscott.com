---

SPDX-License-Identifier: CC-BY-NC-ND-4.0
title: WiFi to Ethernet "Psuedo-Bridge" using Proxy ARP
published: 2100-01-01 00:00:00PDT
is-draft: true

---


<!--more-->

<!--
Hardware
protectli
- overkill. 2.5 gig networking nice, but for wifi bridge not useful unless clsoe enoguh to router. but can achieve more than 1G speed under ideal circuistance. (If for some reason you can do that, but not run a cable)
- the USB-C serial port is really handy; if I mess up networking config, can just use serial. Can also access bios.
-->

<!-- 
Attemping to add a route?
-->

<!--
rust code
not understanding what daemons do differently from kernel
proxy arp seems to work fine; ndp I guess requires explicit config?

want to avoid double NAT for things like tailscale
though other than for peer-to-peer protocols, it's... mostly fine. probably. may be the most reliable and consistently supported solution.

nftables
-->

### Why

* The fun/misery of dealing with networking protocols
* Use WiFi 7 connection on devices that have older WiFi, and/or lack drivers (more an issue with BSDs than Linux); definitely should be better than the USB 2.0 WiFi devices that are most widely supported.
* If I want to connect an SBC or such, I can just connect a cable to the switch

### ARP (Address Resolution Protocol)

<!--
ARP -a
arping
manual arp table entry
advertise
-->

### `/etc/sysctl.conf`

```
net.ipv4.ip_forward=1
net.ipv4.conf.wlan0.proxy_arp=1
net.ipv6.conf.all.forwarding=1
```

<!--
link kernel docs
mention how it suggests forwarding `all`
- can restrict using iptables or nftables as desired

net.ipv6.conf.all.proxy_ndp=1
-->

### `/etc/network/interfaces`

```
auto lo
iface lo inet loopback

auto br0
iface br0 inet manual
	pre-up ip link add name $IFACE type bridge
	address 192.168.2.1/24
	post-down ip link del $IFACE

auto eth0
iface eth0 inet manual
	up ip link set $IFACE up
	up ip link set $IFACE master br0
	down ip link set $IFACE nomaster
	down ip link set $IFACE down

auto wlan0
iface wlan0 inet dhcp
```

Connecting eth0 to an unmanaged switch, we can add as many devices as we want (well, as the switch supports; until the subnet runs out of IPs; etc).

<!-- wpa_supplicant.conf; MLO not working well? -->

### dnsmasq

### What About IPv6?

<!--
Can have publically routable IP.
If we're not able to get a subnet prefix from main router, can't have route
- could use IPv4 for local traffic, but have IPv6 available as well for tailscale, etc.
need a /64 to use SLACC
-->
